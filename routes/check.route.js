var express = require('express');
var router = express.Router();
var authenticate = require('../helpers/passport')
var checkController = require('../controllers/check.controller');

router.route('/')
     .post(authenticate, checkController.validateCreate() , checkController.create )
     .get(authenticate , checkController.findAll );

router.route('/:id')
        .delete(authenticate, checkController.delete)
        .put(authenticate, checkController.validateCreate(),checkController.update)
        .get(authenticate, checkController.getById)

module.exports = router;