var express = require('express');
var router = express.Router();
var authenticate = require('../helpers/passport')
var userController = require('../controllers/user.controller');

router.route('/confirm/:confirmationCode').get(userController.verifyMail)
router.route('/signin').post(userController.validateSignIn(), userController.signIn)
router.route('/')
     .post( userController.validateSignUp() , userController.signUp )
     .get(authenticate , userController.getAll );

router.route('/:id')
        .delete(authenticate, userController.delete)
        .patch(authenticate, userController.validateUpdate(),userController.update)
        .get(authenticate, userController.getById)

module.exports = router;