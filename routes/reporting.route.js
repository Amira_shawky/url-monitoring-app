var express = require('express');
var router = express.Router();
var authenticate = require('../helpers/passport')
var reportingController = require('../controllers/reports.controllers');


router.route('/:id').get(authenticate, reportingController.getById)

module.exports = router;