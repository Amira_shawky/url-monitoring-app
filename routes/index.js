var express = require('express');
var router = express.Router();
var usersRouter = require('./user.route');
var checksRouter = require('./check.route');
var reportsRouter = require('./reporting.route');

router.use('/user',usersRouter);
router.use('/check',checksRouter);
router.use('/report',reportsRouter);



module.exports = router;
