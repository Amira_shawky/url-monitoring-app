const { createClient } = require('redis');

const configs = require('../config/config')

class Reporting {
    constructor(){
        this.connect();
    }

    async connect(){
        this.client = createClient(configs.reportingCash);
        await this.client.connect();
    }

    async getCheckReport(id){
       
        let report = await this.client.hGetAll(''+ id);
        report = report.report ?  JSON.parse(report.report) : {};
        return report;
    }

    async setCheckReport(id, report){
        console.log('id ',report);
        await this.client.hSet(''+ id,'report',JSON.stringify(report) );
    }
}

module.exports = Reporting;