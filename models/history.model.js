const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoInctrement = require('mongoose-auto-increment');

const historySchema = new Schema({
    _id:{
        type:Number
    },
    check:{
        type:Number
    },
    deleted:{
        type:Boolean,
        default:false
    }
},{skipVersioning:true,timestamps:true});

historySchema.set('toJSON',{
    transform: function (doc,ret) {
        delete ret.password;
    }
})


autoInctrement.initialize(mongoose.connection);

historySchema.plugin(autoInctrement.plugin,{startAt:1,model:'history'});
module.exports = mongoose.model('history',historySchema);