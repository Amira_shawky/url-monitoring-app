const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment');


const checkSchema = new Schema({
    id:{
        type:Number
    },
    deleted:{
        type:Boolean,
        default:false
    },
    user:{
        type:Number,
        required:true,
        ref:'user'
    },
    name:{
        type:String
    },
    url:{
        type:String
    },
    protocol:{
        type:String,
        enum:['HTTP', 'HTTPS',  'TCP']
    },
    path:{
        type:String
    },
    port:{
        type:Number
    },
    webhook:{
        type:String
    },
    timeout:{
        type:Number,
        default:5 // sec
    },
    interval:{
        type:Number,
        default:10 // min
    },
    threshold:{
        type:Number,
        default:1
    },
    authentication:{
        username:String,
        password:String
    },
    httpHeaders:[{key:String,value:String}],
    assert:{
        statusCode:Number
    },
    tags:[String],
    ignoreSSL:{
        type:Boolean
    }

},{skipVersioning:true,timestamps:true})

autoIncrement.initialize(mongoose.connection);
checkSchema.plugin(autoIncrement.plugin,{model:'check',startAt:1})

module.exports = mongoose.model('check',checkSchema);