const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoInctrement = require('mongoose-auto-increment');

const userSchema = new Schema({
    _id:{
        type:Number
    },
    name:{
        type:String
    },
    email:{
        type:String,
        required:true
    },
    verified:{
        type:Boolean,
        default:false
    },
    confirmationCode:{
        type:String
    },
    validUntil:{
        type:Date
    },
    password:{
        type:String,
        required:true
    },
    deleted:{
        type:Boolean,
        default:false
    },
},{skipVersioning:true,timestamps:true});




autoInctrement.initialize(mongoose.connection);

userSchema.plugin(autoInctrement.plugin,{startAt:1,model:'user'});
module.exports = mongoose.model('user',userSchema);