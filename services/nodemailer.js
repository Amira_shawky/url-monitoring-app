const nodemailer = require("nodemailer");
const configs = require("../config/config");



const transport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: configs.email,
        pass: configs.password
    },
});

module.exports.sendEmail = async ( email, subject,html) => {

    await transport.sendMail({
        from: configs.email,
        to: email,
        subject: subject,
        html: html,
    })
};