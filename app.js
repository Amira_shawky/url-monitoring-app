var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
const i18n = require('i18n')
var configs = require('./config/config');
var app = express();


if (process.env.isTask) {
  console.log('ttttttttttttttttttttttttt');
  const BullQueue = require('./controllers/bull-queue.controller');
  const queue = new BullQueue();
  queue.start()
}









i18n.configure({
  locales: ['en', 'ar'],
  directory: path.join(__dirname, '/locales'),

})

mongoose.connect(configs.dbUrl);
mongoose.connection.on('connected',()=>{



  autoIncrement.initialize(mongoose.connection);
  console.log('DB connected.');
})

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use((req,res,next)=>{
  i18n.setLocale(req.headers['accept-language'] || 'ar');
  return next();
})

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page

  console.log('err ',err.status );

  res.status(err.status || 500).send({error:err.message});
});

module.exports = app;
