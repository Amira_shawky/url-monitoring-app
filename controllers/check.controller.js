

const Check = require('../models/check.model');
const i18n = require('i18n');
const {body} = require('express-validator');
const {checkValidations} = require('../helpers/helpers');
const  ValidationError  = require('../helpers/validation-error');
const JobQueue = require('./bull-queue.controller');
const queue = new JobQueue();


module.exports = {

    validateCreate(){
        return [
            body('name').not().isEmpty().withMessage(()=>i18n.__('nameRequired')),
            body('url').not().isEmpty().withMessage(()=>i18n.__('urlRequired')),
            body('protocol').not().isEmpty().withMessage(()=>i18n.__('protocolRequired'))
                            .isIn(['HTTP', 'HTTPS',  'TCP']),
            body('path').optional().not().isEmpty().withMessage(()=>i18n.__('pathRequired')),
            body('port').optional().not().isEmpty().withMessage(()=>i18n.__('portRequired')),
            body('webhook').optional().not().isEmpty().withMessage(()=>i18n.__('webhookRequired')),
            body('timeout').optional().not().isEmpty().withMessage(()=>i18n.__('timeoutRequired')),
            body('interval').optional().not().isEmpty().withMessage(()=>i18n.__('intervalRequired')),
            body('threshold').optional().not().isEmpty().withMessage(()=>i18n.__('thresholdRequired')),
            body('authentication').optional().not().isEmpty().withMessage(()=>i18n.__('authenticationRequired')),
            body('authentication.username').optional().not().isEmpty().withMessage(()=>i18n.__('authenticationUserNameRequired')),
            body('authentication.password').optional().not().isEmpty().withMessage(()=>i18n.__('authenticationPasswordRequired')),

            body('httpHeaders').optional().not().isEmpty().withMessage(()=>i18n.__('httpHeadersRequired')).isArray().withMessage(()=>i18n.__('invalidHttpHeaders')),
            body('httpHeaders.*.key').not().isEmpty().withMessage(()=>i18n.__('httpHeadersKeyRequired')),
            body('httpHeaders.*.value').not().isEmpty().withMessage(()=>i18n.__('httpHeadersKeyRequired')),

            body('assert').optional().not().isEmpty().withMessage(()=>i18n.__('assertRequired')),
            body('assert.statusCode').not().isEmpty().withMessage(()=>i18n.__('assertStatusCodeRequired')),

            body('tags').optional().not().isEmpty().withMessage(()=>i18n.__('tagsRequired'))
                .isArray().withMessage(()=>i18n.__('invalidTags')),

            body('ignoreSSL').not().isEmpty().withMessage(()=>i18n.__('ignoreSSLRequired'))


        ]
    },

    async create(req,res,next){
        try {
            let user = req.user;
            let data = checkValidations(req);
            data.user = user._id;
            let check = await Check.create(data)
            check = await Check.populate(check,[{path:'user',model:'user'}]);
            res.status(200).send({check});

            queue.add(check);
        } catch (error) {
            console.log(error);
            next(error)   
        }
    },

    async update(req,res,next){
        try {
            let {id} = req.params;
            let user = req.user;
            let check = await Check.findOne({deleted:false,_id:id});
            if (!check) {
                return next(new ValidationError(404,i18n.__('checkNotfound')));
            }
            if (check.user != user._id) {
                return next(new ValidationError(403,i18n.__('unauthrized')));
            }
         
            let data = checkValidations(req);

            check = await Check.findByIdAndUpdate(id, data,{new:true});
            res.status(200).send({check});
        } catch (error) {
            next(error)   
        }
    },
    async delete(req,res,next){
        try {
            const {id} = req.params;
            let user = req.user;

            let check = await Check.findOne({deleted:false,_id:id});
            if (!check) {
                return next(new ValidationError(404,i18n.__('checkNotfound')));
            }
            if (check.user != user._id) {
                return next(new ValidationError(403,i18n.__('unauthrized')));
            }
            check.deleted = true;
            await check.save();
            res.status(200).send({status:'Deleted'});
        } catch (error) {
            next(error)   
        }
    },
    async getById(req,res,next){
        try {
            const {id} = req.params;
            let user = req.user;
            
            let check = await Check.findOne({deleted:false,_id:id});
            if (!check) {
                return next(new ValidationError(404,i18n.__('checkNotfound')));
            }
            if (check.user != user._id) {
                return next(new ValidationError(403,i18n.__('unauthrized')));
            }
            res.status(200).send({check});
        } catch (error) {
            next(error)   
        }
    },

    async findAll(req,res,next){
        try {
            let user = req.user;
            let checks = await Check.find({user:user.id, deleted:false});
            res.status(200).send({checks});
        } catch (error) {
            next(error)   
        }
    }

}