const scheduler = require('node-schedule');
const nodemailer = require('../services/nodemailer');
const Reporting = require('../models/reports.model');
const History = require('../models/history.model');
const reporting = new Reporting();
const axios = require('axios');
const https = require('https');
const httpsAgent = new https.Agent({  
    rejectUnauthorized: false
})



class Monitoring {

    monitor(check) {
        try {
            let self = this;

            let cron = `*/${check.interval} * * * *`;
            const job = scheduler.scheduleJob('check-' + check._id, cron, async function (fireDate) {
                console.log('fire Date ',fireDate);
                self.process(check,self);
                return Promise.resolve();
            });

        } catch (error) {
            console.log('fail to monitor');
        }
    }


    async process(check,self) {
        try {
           
            let req = self.pollConfig(check);
            let startTime = new Date().getTime();
            let { error, data } = await self.axiosCall(req);
            let endTime = new Date().getTime();

            let responseTime = endTime - startTime;

            let report = await reporting.getCheckReport(check._id);

            if (Object.keys(report).length == 0) {
                report = { currentThreshold: 0, uptime: 0, downtime: 0, pollsCount: 0, totalResponseTime: 0, responseTime: 0, availabilityCount: 0, outages: 0 };
            }

            report.pollsCount = report.pollsCount + 1;
            report.totalResponseTime = report.totalResponseTime + responseTime;
            report.responseTime = report.totalResponseTime / report.pollsCount;

            if (!error) {
                report.status = 'UP';
                report.availabilityCount = report.availabilityCount + 1;
                report.availability = report.availabilityCount / report.pollsCount;
                report.uptime = check.interval;
            } else {
                report.status = 'DOWN';
                report.outages = report.outages + 1;
                report.downtime = check.interval;
                report.currentThreshold = report.currentThreshold + 1;
                if (report.currentThreshold >= check.threshold) {

                    let html = `<h1>Monitoring Task Alert</h1>
                    <h2>Hello ${check.user.name}</h2>
                    <p>Thank you for subscribing. Your check has reported a new failer.</p>
                    </div>`
                    report.currentThreshold = 0;
                    await nodemailer.sendEmail(check.user.email, `${check.name} Failer`,html)
                }
            }

            await reporting.setCheckReport(check._id, report);
            await History.create({})
        } catch (error) {
            console.log('error crone ',error);
        }

    }


    async axiosCall(req) {
        try {
            console.log('new axios call ', req);

            let { data } = await axios(req);
            return { data }
        } catch (error) {
            console.log('error in polling ', error);
            return { error }


            // if (error.response) {
            //     let { status, data } = error.response;
            //     status ? ctx.setError(this.efinanceErrors.level, this.efinanceErrors.code.providerServerErorr, data || error.stack) :
            //         ctx.setError(this.codeDefect.level, this.codeDefect.code.Deferr100, data || error.stack)
            // } else if (error.request) {
            //     let { code } = error;
            //     // (code && code === "ECONNABORTED") ? ctx.setError(this.efinanceErrors.level, this.efinanceErrors.code.timeout, error.message || error.stack) :
            //     ctx.setError(this.efinanceErrors.level, this.efinanceErrors.code.connectionError, error.message || error.stack)
            // } else {
            //     // Something happened in setting up the request that triggered an Error 
            //     ctx.setError(this.codeDefect.level, this.codeDefect.code.Deferr100, error.stack)
            // }

        }
    }

    async pollConfig(check) {
        let baseURL = `${check.protocol}://${check.url}${check.port ? `:${check.port}` : ''}/${check.path?`${check.path}`:'/'}`;
        

        let headers = {};
        let headerLength = check.httpHeaders.length;

        for (let index = 0; index < headerLength; index++) {
            headers[check.httpHeaders[index].key] = check.httpHeaders[index].value;
        }

        if (check.ignoreSSL) {
            headers.httpsAgent = httpsAgent;
        }

        let req = {
           
            url:baseURL,
            timeout: check.timeout,
            headers
        }

        if (check.authentication) {
            req.auth = check.authentication;
        }

        return req;
    }

}


module.exports = Monitoring;