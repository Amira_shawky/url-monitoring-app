
const { body } = require('express-validator');
const { checkValidations, generateJwtToken } = require('../helpers/helpers');
const i18n = require('i18n');
const User = require('../models/user.model');
const ValidationError = require('../helpers/validation-error');
const configs = require('../config/config');
const nodemailer = require('../services/nodemailer');
const jsonwebtoken = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const sendVerifyEmail = async(name, confirmationCode,email) => {
    let html = `<h1>Email Confirmation</h1>
    <h2>Hello ${name}</h2>
    <p>Thank you for subscribing. Please confirm your email by clicking on the following link</p>
    <a href=http://localhost:3000/user/confirm/${confirmationCode}> Click here</a>
    </div>`


    await nodemailer.sendEmail(email, "Please confirm your account", html);
}


module.exports = {


    validateSignIn() {
        return [
            body('password').not().isEmpty().withMessage(() => i18n.__('passwordRequired')),
            body('email').not().isEmpty().withMessage(() => i18n.__('emailRequired'))
                .isEmail().withMessage(() => i18n.__('invalidEmail'))

        ]
    },


    async signIn(req, res, next) {
        try {

            let data = checkValidations(req);
            data.email = data.email.trim().toLowerCase();

            let user = await User.findOne({ email: data.email });
            if (!user) {
                return next(new ValidationError(404, 'userNotFound'));
            }

            if (!user.verified) {
                await sendVerifyEmail(user.name, user.confirmationCode,user.email);
                return next(new ValidationError(400, 'notverified'));
            }
            let compare = await bcrypt.compare(data.password,user.password);
            console.log('compare ' , data.password,user.password ,compare);
            if (!compare) {
                return next(new ValidationError(404, 'userNotFound'));
            }

            let token = generateJwtToken(user);

            res.status(200).send({ user, token });
        } catch (error) {
            next(error);
        }
    },

    validateSignUp() {
        return [
            body('password').not().isEmpty().withMessage(() => i18n.__('passwordRequired')),
            body('name').not().isEmpty().withMessage(() => i18n.__('nameRequired')),
            body('email').not().isEmpty().withMessage(() => i18n.__('emailRequired'))
                .isEmail().withMessage(() => i18n.__('invalidEmail'))
                .custom(async (email) => {
                    email = email.trim().toLowerCase();
                    let old = await User.findOne({ deleted: false, email });
                    if (old) {
                        throw new ValidationError(422, i18n.__('duplicatedEmail'));
                    }
                    return true
                })
                .withMessage(() => i18n.__('duplicatedEmail'))
        ]
    },



    async signUp(req, res, next) {
        try {
            let data = checkValidations(req);
            data.email = data.email.trim().toLowerCase();

            data.confirmationCode = jsonwebtoken.sign({ email: data.email }, configs.jwtSecret);


            const salt = bcrypt.genSaltSync();
            data.password = await bcrypt.hash(data.password, salt)
            console.log(data.password);

            let user = await User.create(data);


            await sendVerifyEmail(user.name, user.confirmationCode,user.email);

            res.status(200).send({ message: 'User was registered successfully! Please check your email' });
        } catch (error) {
            next(error);
        }
    },

    async verifyMail(req, res, next) {
        try {
            let { confirmationCode } = req.params;
            let user = await User.findOne({ confirmationCode: confirmationCode });
            if (!user) {
                return next(new ValidationError(404, 'userNotFound'));
            }

            user.verified = true;
            await user.save();

            let token = generateJwtToken(user);

            res.status(200).send({ user, token });
        } catch (error) {
            next(error);
        }
    },

    async delete(req, res, next) {
        try {
            const { id } = req.params;
            let user = req.user;
            console.log('user._id ', user._id, user.id);
            if (user._id != id) {
                throw new ValidationError(403, i18n.__('unauthrized'));
            }
            user.deleted = true;
            await user.save();
            res.status(200).send({ status: 'Deleted' });
        } catch (error) {
            next(error)
        }
    },

    async getById(req, res, next) {
        try {
            const { id } = req.params;
            let user = req.user;

            if (user._id != id) {
                throw new ValidationError(403, i18n.__('unauthrized'));
            }
            if (user.deleted) {
                throw new ValidationError(404, i18n.__('userNotFound'));
            }


            res.status(200).send({ user });
        } catch (error) {
            next(error)
        }
    },

    async getAll(req, res, next) {
        try {
            let users = await User.find({ deleted: false });
            res.status(200).send({ users });
        } catch (error) {
            next(error);
        }
    },

    validateUpdate() {
        return [
            body('password').optional().not().isEmpty().withMessage(() => i18n.__('passwordRequired')),
            body('name').optional().not().isEmpty().withMessage(() => i18n.__('nameRequired')),
            body('email').optional().not().isEmpty().withMessage(() => i18n.__('emailRequired'))
                .isEmail().withMessage(() => i18n.__('invalidEmail'))
        ]
    },

    async update(req, res, next) {
        try {

        } catch (error) {
            next(error);
        }
    }

}