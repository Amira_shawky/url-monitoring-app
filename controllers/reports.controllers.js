


const Check = require('../models/check.model');
const Reporting = require('../models/reports.model');
const ValidationError = require('../helpers/validation-error')
const reporting = new Reporting();
const i18n = require('i18n');

module.exports = {

    async getById(req,res,next){
        try {
            const {id} = req.params;
            let user = req.user;
            
            let check = await Check.findOne({deleted:false,_id:id});
            if (!check) {
                return next(new ValidationError(404,i18n.__('checkNotfound')));
            }
            if (check.user != user._id) {
                return next(new ValidationError(403,i18n.__('unauthrized')));
            }
            let report = await reporting.getCheckReport(id);

            if (Object.keys(report).length == 0) {
                return next(new ValidationError(404,i18n.__('noReportAvailable')));
            }

            res.status(200).send({report});
        } catch (error) {
            next(error)   
        }
    }
}