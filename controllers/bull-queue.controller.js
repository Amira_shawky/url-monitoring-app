const Queue = require('bull');
const configs = require('../config/config');
const Monitoring = require('../controllers/monitoring.controller');
const monitoring = new Monitoring()

class JobQueue {

    constructor() {
        this.checkQueue = new Queue('checksQueue', configs.bullRedis);
    }

    start() {
        this.checkQueue.process(async function ( job) {
            monitoring.monitor(job.data);
        });
    }

    add(check) {
        try {
            this.checkQueue.add( check );
        } catch (error) {
            console.log(error);
        } 
    }
}

module.exports = JobQueue
