
const {validationResult,matchedData} = require('express-validator');
const jsonwebtoken = require('jsonwebtoken');
const ValidationError = require('./validation-error');
const {jwtSecret} = require('../config/config');

module.exports = {

    checkValidations(req) {

        const validationErrors = validationResult(req).array({ onlyFirstError: true });

        if (validationErrors.length > 0) {
            throw new ValidationError(422,validationErrors[0].msg);
        }
        return matchedData(req);
    },

    generateJwtToken(user){
        const token = jsonwebtoken.sign({sub:user._id},jwtSecret,{expiresIn:'24h'});
        return token;
    }

}